import wget as wget
from tqdm import tqdm

for i in tqdm(range(1)):
    mainurl = 'https://bitbucket.org/BLINMATIC/safeos/raw/da66c8219e025b09995761e8fdf1f7f15205a8ae/__main__.py'
    appsurl = 'https://bitbucket.org/BLINMATIC/safeos/raw/da66c8219e025b09995761e8fdf1f7f15205a8ae/apps.py'
    terminalurl = 'https://bitbucket.org/BLINMATIC/safeos/raw/da66c8219e025b09995761e8fdf1f7f15205a8ae/terminal.py'
    readmeurl = 'https://bitbucket.org/BLINMATIC/safeos/raw/da66c8219e025b09995761e8fdf1f7f15205a8ae/README.md'
    __main__ = wget.download(mainurl)
    apps = wget.download(appsurl)
    terminal = wget.download(terminalurl)
    readme = wget.download(readmeurl)
